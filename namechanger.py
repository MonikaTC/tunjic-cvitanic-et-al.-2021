import os

def changer(filename, index, outdir):
   with open(filename, mode='r') as f:
      data = f.readlines()

   row = data[0].split()

   newrow = '>' + str(index) + ' ' + row[1] + '\n'

   row1= True

   with open(outdir, "w+") as f:
      for line in data:
         if row1:
            f.write(newrow)
            row1 = False
         else:
            f.write(line)



   

def get_filenames(dirname):
   l = os.listdir(dirname)
   l.remove('scaffolds.csv_positions.txt')
   return ( l)

if __name__ == '__main__':
   DIR = "cgisat01b na scaffolds"
   OUTDIR = "output"

   #create outdir if it does not exist
   if not os.path.exists(OUTDIR):
      os.makedirs(OUTDIR)

   lista = get_filenames(DIR)
   
   for i in range(len(lista)):
      changer(os.path.join(DIR,lista[i]), i, os.path.join(OUTDIR, lista[i]))
   print("Done!")