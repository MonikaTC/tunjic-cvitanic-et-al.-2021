import os


def file_worker(filename, LIST, LIST_max):
    """ Parses file and fills LIST container"""
    f = open(filename, "r")
    f.readline() #get rid of first line (column names)
    for line in f:
        line = line.split(",")
        if line[0] == '\n':
            continue
        nr = int(line[0])-22994773
        index = line[2].find("Box")
        if index >= 0:
            box = int(line[2][index+4])-1

            LIST[nr][box] = 1

        if nr > LIST_max:
            LIST_max = nr
    f.close()
    return (LIST, LIST_max)

def first_writer(filename, LIST, LIST_max, header = True):
    """Writes data for LIST to first csv"""
    f = open(filename, "w+")
    if header:
        f.write("Position,Box 1,Box 2,Box 3,Box 4,Box 5,Box 6,Box 7\n")

    for i in range(LIST_max+1):
        line = str(i)
        for j in range(7):
            line += ","+str(LIST[i][j])
        line += "\n"
        f.write(line)
    f.close()

def second_writer(filename, LIST, LIST_max, header = True):
    """Writes data per boxes"""
    f = open(filename, "w+")

    boxes = ["Box 1", "Box 2", "Box 3", "Box 4", "Box 5", "Box 6", "Box 7"]

    f.write("Box,List of positions\n")

    for i, box in enumerate(boxes):
        f.write(box)
        for j in range(LIST_max):
            if LIST[j][i] == 1:
                f.write( ","+str(j) )
        f.write("\n")
    f.close()


if __name__ == "__main__":

    FILENAME = "anotacije boxeva na scaffolde cgisat03.csv"

    FILENAME1 = FILENAME[:-4]+"_first.csv"
    FILENAME2 = FILENAME[:-4]+"_second.csv"

    LIST = [[0]*7 for i in range(10000)]
    LIST_max = 0
    LIST, LIST_max = file_worker(FILENAME,LIST, LIST_max)
    first_writer(FILENAME1, LIST, LIST_max)
    second_writer(FILENAME2,LIST,LIST_max)