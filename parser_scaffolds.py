import os

def parser_scaffold(filename,satname,scaffold, d):
   f = open(filename, mode='r')
   f.readline()

   positions = []
   last_min = 0
   last_max = 0
   first = True

   for line in f:
      parts = line.split(',')
      line_scaffold = parts[0]
      name = parts[1]

      if scaffold == line_scaffold:
         if name == satname:
            #print("hit")
            try:
               current_max = int(parts[4])
               current_min = int(parts[3])
            except:
               continue
            positions.append( (current_min, current_max) )

   positions.reverse()
   sequences = []
   sequence_start = 0
   sequence_end = 0

   for pos in positions:
      if first:
         sequence_start = pos[0]
         first = False
      else:
         if (pos[0]-last_max) < d:
            pass
         else:
            sequence_end = last_max
            sequences.append( (sequence_start, sequence_end) )
            sequence_start = pos[0]
      last_min = pos[0]
      last_max = pos[1]

   sequence_end = last_max
   sequences.append( (sequence_start, sequence_end) )

   f.close()
   return sequences

def finder_scaffold(cfilename, sequences, scafflist, L):
   cfile = open(cfilename, mode='r')
   cfile.readline()
   foldername = "Output_files"

   if not os.path.exists(foldername):
      os.makedirs(foldername)

   nfile = open(foldername + '/' + cfilename + '_positions.txt','w+')

   for scaf in sequences:
      cline = cfile.readline().split(',')
      scafname = cline[0]
      cline_strip = cline[1]

      for i,seq in enumerate(scaf):
         #ovdje promijeni zelis li zadrzati sredinu ili ne:
         KEEP_MIDDLE = True #True ili False
         start = seq[0]-L
         if start < 0:
            start = 0
         end = seq[1]+L
         size = seq[1]-seq[0]
         nfile.write(scafname + ' ' + str(start) + ' ' + str(end) + ' ' + str(size) + '\n')
         if KEEP_MIDDLE:
            txt = cline_strip[start : end]
         else:
            txt = cline_strip[start : (start+L)]
            txt = txt + cline_strip[seq[0] : (seq[0]+L)]
         sfile = open(foldername + '/' + scafname + '_out' + str(i) + '.fasta','w+')
         sfile.write('>'+ scafname + ' ' + str(i)+'\n')
         sfile.write(txt)
         sfile.close()

   nfile.close()
   cfile.close()

def get_scaffolds(sfilename):
   f = open(sfilename, "r")
   f.readline() #skip header
   names = []
   for line in f:
      result = line.find(',')
      names.append(line[0:result])

   f.close()
   return names


if __name__ == "__main__":
   print("Test program")
   ANOTNAME = "anotacije scaffolds AT.csv"
   SCFLDNAME = "scaffolds.csv"

   scafold_list = get_scaffolds(SCFLDNAME)

   p = []

   print("Starting parser...")
   for item in scafold_list:
      #print("Starting parser for scaffold", item)
      p.append( parser_scaffold(filename=ANOTNAME, satname='CgiSat01b',scaffold=item, d=500) )
      #print("Parser done")

   print("parser done!")
   finder_scaffold(SCFLDNAME, p,scafold_list, 1000)
   print("Program done!")
