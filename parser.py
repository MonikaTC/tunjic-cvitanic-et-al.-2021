import os

def parser(filename,satname,d):
   f = open(filename, mode='r')
   f.readline()

   positions = []
   last_min = 0
   last_max = 0
   first = True

   for line in f:
      parts = line.split(',')
      name = parts[0]

      if name == satname:
         #print("hit")
         current_max = int(parts[3])
         current_min = int(parts[2])
         positions.append( (current_min, current_max) )

   positions.reverse()
   sequences = []
   sequence_start = 0
   sequence_end = 0

   for pos in positions:
      if first:
         sequence_start = pos[0]
         first = False
      else:
         if (pos[0]-last_max) < d:
            pass
         else:
            sequence_end = last_max
            sequences.append( (sequence_start, sequence_end) )
            sequence_start = pos[0]
      last_min = pos[0]
      last_max = pos[1]

   sequence_end = last_max
   sequences.append( (sequence_start, sequence_end) )

   f.close()
   return sequences

def finder(cfilename, sequences, L):
   cfile = open(cfilename, mode='r')
   foldername = "Output_files"

   if not os.path.exists(foldername):
      os.makedirs(foldername)

   nfile = open(foldername + '/' + cfilename + '_positions.txt','w+')

   for i,seq in enumerate(sequences):
      #ovdje promijeni zelis li zadrzati sredinu ili ne:
      KEEP_MIDDLE = True #True ili False
      start = seq[0]-L + 10
      if start < 0:
         start = 0
      end = seq[1]+L + 10
      size = seq[1]-seq[0]
      nfile.write(str(start) + ' ' + str(end) + ' ' + str(size) + '\n')
      cfile.seek(start)
      if KEEP_MIDDLE:
         txt = cfile.read(end-start)
      else:
         txt = cfile.read(L)
         cfile.seek(seq[0])
         txt = txt + cfile.read(L)
      sfile = open(foldername + '/' + cfilename + '_out' + str(i) + '.fasta','w+')
      sfile.write('>'+str(i)+'\n')
      sfile.write(txt)
      sfile.close()

   nfile.close()
   cfile.close()


print("Test program")
ANOTNAMES = ["C1A.csv","C2A.csv", "C3A.csv", "C4A.csv", "C5A.csv", "C6A.csv", "C7A.csv", "C8A.csv", "C9A.csv", "C10A.csv"]
CMSMNAMES = ["C1.csv","C2.csv", "C3.csv", "C4.csv", "C5.csv", "C6.csv", "C7.csv", "C8.csv", "C9.csv", "C10.csv"]
if len(ANOTNAMES) == len(CMSMNAMES):
   for i in range(len(ANOTNAMES)):
      print(ANOTNAMES[i],CMSMNAMES[i])
      p = parser(filename=ANOTNAMES[i], satname='CgiSat01a', d=500)
      print("Parser done")
      print(p[0])
      finder(CMSMNAMES[i], p, 1000)
      print(CMSMNAMES[i]+" completed")
   print("Program done")
